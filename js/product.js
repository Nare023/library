document.addEventListener("DOMContentLoaded", function() {
    // Get the cart badge element
    const cartBadge = document.getElementById('cart-badge');
    let initialValue = 0;

    // Set the initial value (e.g., if you have items already in the cart)
    // Replace 'initialValue' with the actual initial value of the cart
    cartBadge.textContent = initialValue; // e.g., initialValue = 0;

    // Select all elements with the class 'btn-outline-dark' within the section with the id 'productsSection'
    const addToCartButtons = document.querySelectorAll('#productsSection .btn-outline-dark');

    addToCartButtons.forEach(button => {
        button.addEventListener('click', function(event) {
            // Prevent the default form submission behavior
            event.preventDefault();
            // Increment the value when an item is added to the cart
            let currentValue = parseInt(cartBadge.textContent);
            cartBadge.textContent = currentValue + 1;
        });
    });
});
